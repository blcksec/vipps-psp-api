<!-- START_METADATA
---
title: Introduction
sidebar_position: 1
hide_table_of_contents: true
pagination_next: null
pagination_prev: null
---
END_METADATA -->

# Vipps PSP API

Settlements for Payment Service Provider (PSP) integrations are handled by the PSP, but you can use the Vipps PSP API to initiate PSP payments, get details about PSP payments, or update their status.
In addition, you can use the PSP Signup API to get information about merchant sale units or create new sale units.

For product information in Norwegian, see
[Vipps på Nett via PSP](https://vipps.no/produkter-og-tjenester/bedrift/ta-betalt-paa-nett/ta-betalt-paa-nett/#kom-i-gang-med-vipps-pa-nett-category-2).

<!-- START_COMMENT -->

💥 Please see the new documentation pages here: [Vipps Technical Documentation](https://vippsas.github.io/vipps-developer-docs/).

<!-- END_COMMENT -->

## Getting started

See
[Getting Started](https://vippsas.github.io/vipps-developer-docs/docs/vipps-developers/vipps-getting-started)
for information about API keys, product activation, how to make API calls, etc.

## PSP API

* [API Quick Start](vipps-psp-api-quick-start.md):  Quick start.
* [API guide](vipps-psp-api.md): Developer guide for Vipps PSP API.
* [API checklist](vipps-psp-api-checklist.md): Checklist for PSP integrations.
* [API FAQ](vipps-psp-api-faq.md): Questions and answers.
* [API reference](https://vippsas.github.io/vipps-developer-docs/api/psp): API specification.

## PSP Signup API

* [API Quick Start](vipps-psp-api-quick-start.md):  Quick start.
* [Signup API guide](vipps-psp-signup-api.md): Developer guide for PSP Signup API.
* [API reference](https://vippsas.github.io/vipps-developer-docs/api/psp-signup): API specification.

## Questions?

We're always happy to help with code or other questions you might have!
Please create an [issue](https://github.com/vippsas/vipps-psp-api/issues),
a [pull request](https://github.com/vippsas/vipps-psp-api/pulls),
or [contact us](https://vippsas.github.io/vipps-developer-docs/docs/vipps-developers/contact).

Sign up for our [Technical newsletter for developers](https://vippsas.github.io/vipps-developer-docs/docs/vipps-developers/newsletters).
